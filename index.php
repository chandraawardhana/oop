<?php

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');


//Release 0
$sheep = new Animal("shaun");
echo "$sheep->name <br>" ; // "shaun"
echo "$sheep->legs <br>"; // 2
echo "$sheep->cold_blooded <br> <p>"; // false


//Release 1
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"


